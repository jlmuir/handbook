---
layout: page
title: Compensation
weight: 2
---

Compensation for each role is a combination of salary, equity, and benefits.

## Salary

The salary for a role is determined using market data from [Option Impact](https://www.optionimpact.com/) that takes into account the stage of our company (Seed Funding Only), the industry we're in (Enterprise), and the level of the role (Junior, Intermediate, Senior, etc).

[Where you work from](/company/all-remote#where-we-hire) is also a factor.
We aim to pay highly competitively (at the top or above market) everywhere by
dividing the world up into a limited number of regions,
and using this same market data to determine a salary that is competitive in the entire region.

Specifically, we determine the lower and upper bounds for regional salaries by
taking the top-of-the-market (75th-90th percentile) salaries for the role in San Francisco, and
multiplying them with a region-specific factor that is determined by comparing the SF salaries with salaries in the region's major cities (that represent its top-of-the-market).

For roles at the Head/Director level and up, we pay US salaries regardless of the region,
as the top-of-the-market talent pool at these levels is relatively small and as a result regional differences in salary are as well.

## Equity

As an early-stage company, we expect every team member to have a huge impact on our success. We want to make sure that everyone has a financial stake in the success and that contributions are rewarded.

The equity (stock options) for a given role and level is determined using the same market data used for the [salary](#salary), but there is no difference between regions and the San Francisco benchmark is used wherever you are based.

Specifically, we divide the 75th percentile Gross Equity Value by the latest valuation of the company to determine the appropriate ownership percentage, which is then multiplied by the total number of shares to determine the number of options to be granted.

Options vest monthly over 4 years, with a 1-year cliff. Early exercise is allowed in countries where this has potential tax benefits, like the US.

## Bonuses

### Discretionary Bonuses for Individuals
 - Every now and then, individual team members really shine as they live
   our values and impact the company positively. We especially like to
   celebrate the special moments that exemplify the behavior we want to
   see in all Meltano team members.
   
 - Anyone can nominate any full-time employee for a discretionary bonus.
   Please contact the person’s manager to do so.
 - Bonuses are at the fixed amount of $1,000.
 - There is no limit to the frequency with which someone can receive a
   bonus but they cannot get more than one for the same activity/project for the same reasons.
 - Only individuals in good standing with the company are eligible for discretionary bonuses (I.E. not currently undergoing a performance improvement plan).
    

#### Valid and Invalid criteria for discretionary bonuses

Discretionary bonuses are to celebrate team members. To make sure we are recognizing the right behaviors, below are some criteria to help you decide if a bonus meets the requirements.

**Valid bonus criteria**

-   Going above and beyond what is expected in regard to the team member's role description.
    
-   Truly exceptional work. Good and great is already expected in a team member's work and performance.
    
-   Providing additional coverage or project load for other team members.
    
-   Team members' work results in higher productivity or improved processes that create efficiencies and results. This may also result in identifying cost-saving initiatives.
    

**Invalid bonus criteria**

-   Supplement to income.
    
-   For being nice and friendly.
    
-   For being helpful.
    
-   Doing a project or task that is core to their role.
    
-   For help onboarding a new Meltano team member.
    
-   Hitting target or quota
    
-   Providing customer insight
    
-   Partnering or collaborating with other groups within Meltano
    
-   Working long hours or on weekends

## Benefits

See the page on [Benefits](benefits).
