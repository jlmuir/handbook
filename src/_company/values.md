---
layout: page
weight: 2
title: Values
---

Meltano's core values are empathy, community, sustainability, transparency, iteration, ambition, and accountability.

We view our values as supporting each other.
We build a strong foundation by bringing **empathy** to everything we do so that we can build with the **community** in a **sustainable** manner for years to come.
With this foundation, we work **transparently** and **iteratively** to achieve our goals.
Our goals are **ambitious** and we hold ourselves and each other **accountable** for our results.

Our values act as a guide in how we conduct day-to-day business, make decisions, hire, and build our product. While we use these values as our compass, we adjust them based on lessons we've learned and our experiences using them in practice. Anyone can suggest improvements to our values - from team members to internet friends - and we're always open to growing and learning.

## Empathy

We work with empathy, recognizing we're not alone in this journey.
We bring kindness, humility, trust, honesty, and an assumption of good faith to all of our interactions.

## Community

We work to build a more diverse and inclusive community by hiring globally and creating a culture that supports team members everywhere.
We want to enable everyone to contribute - from team members to users - and we recognize that our vibrant community is at the core of Meltano.
We will always keep the interests of our open source users and contributors in mind as we continue to grow.

## Sustainability

We work with a sustainable mindset so that we can continue to grow for years to come.
We believe in investing in our people, community, and products for the long term.
We do this by putting family and friends first, by being wise with resources, and by not adopting a short-term point of view.
Sustainability plays a role in all aspects of our business - from hiring and developing to selecting swag options.
We should strive to be a net positive company that makes net positive actions.
Every change should be towards making "the thing" a little bit better than the one that came before.

## Transparency

Transparency cultivates honesty which, in turn, builds trust.
Without trust, it's impossible to live our other values.
In practice, transparency can be seen in how we discuss decisions, document our work, and encourage everyone to share their thoughts and provide feedback.
We work transparently, because we're passionate about involving our community in everything we're doing.
We strive to provide users with the same level of insight as our team members so that they're able to have full visibility in our work and contribute their perspectives, since anything we build will be better if we collaborate with the people who will use it.

## Iteration

We work in small iterations so that we can be efficient, add value quickly, and get feedback from the community.
We have a bias for action and aim to continually improve our product and processes, and we believe small, positive steps are the best way to achieve large goals.
In practice, we ship minimum viable changes, and we continually seek to improve ourselves.

## Ambition

We have high aspirations and the drive to exceed them.
We want to collaborate with those who are inspired and motivated by our mission.
We’re determined to realize our goals and achieve strong results in everything we set out to accomplish.
To us, ambition means:
* Resilience in the face of setbacks
* Continual learning
* A strong sense of purpose and priorities
* Passion
* Perseverance
* Calculated risk taking

## Accountability

We have a responsibility to ourselves, our teammates, and our community to do our best.
We hold ourselves and each other to high standards, and we're responsible for our work, words, and actions.
We're accountable for our mistakes, but we focus on learning from them and move forward together.
